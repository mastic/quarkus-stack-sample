package com.example;

import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

import io.quarkus.test.junit.NativeImageTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

@NativeImageTest
public class NativeSomeEntityResourceTest {

  @Test
  public void getAll() {
    get("/some-entities")
        .then()
        .statusCode(200);
  }

  @Test
  public void getSingleMissing() {
    get("/some-entities/{id}", 404L)
        .then()
        .statusCode(404);
  }

  @Test
  public void create() {
    SomeEntity body = new SomeEntity(42L, 17983, false, SomeType.values()[0], java.time.Instant.now());
    given()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/some-entities")
        .then()
        .statusCode(201);
  }

  @Test
  public void updateMissing() {
    SomeEntity body = new SomeEntity(42L, 17983, false, SomeType.values()[0], java.time.Instant.now());
    given()
        .contentType(ContentType.JSON)
        .body(body)
        .put("/some-entities/{id}", 42L)
        .then()
        .statusCode(404);
  }

  @Test
  public void deleteSingleMissing() {
    delete("/some-entities/{id}", 404L)
        .then()
        .statusCode(404);
  }

}
