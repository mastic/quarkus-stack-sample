package com.example;

import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

import io.quarkus.test.junit.NativeImageTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

@NativeImageTest
public class NativeFruitResourceTest {

  @Test
  public void getAll() {
    get("/fruits")
        .then()
        .statusCode(200);
  }

  @Test
  public void getSingleMissing() {
    get("/fruits/{id}", 404L)
        .then()
        .statusCode(404);
  }

  @Test
  public void create() {
    Fruit body = new Fruit(42L, "Malayan Tapir");
    given()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/fruits")
        .then()
        .statusCode(201);
  }

  @Test
  public void updateMissing() {
    Fruit body = new Fruit(42L, "Malayan Tapir");
    given()
        .contentType(ContentType.JSON)
        .body(body)
        .put("/fruits/{id}", 42L)
        .then()
        .statusCode(404);
  }

  @Test
  public void deleteSingleMissing() {
    delete("/fruits/{id}", 404L)
        .then()
        .statusCode(404);
  }

}
