package com.example;

import static io.restassured.RestAssured.delete;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.vertx.mutiny.sqlclient.Pool;
import java.net.URI;
import java.util.stream.Stream;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class FruitResourceTest {

  @Inject
  Pool client;

  private Long idToGet, idToUpdate, idToDelete;

  @BeforeEach
  public void initDatabase() {
    client.query("SELECT * FROM FRUIT")
        .execute()
        .onFailure()
        .recoverWithUni(() -> client
            .query("CREATE TABLE FRUIT (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")
            .execute())
        .await()
        .indefinitely();
    idToGet = createOne(new Fruit(null, "Malayan Tapir"));
    idToUpdate = createOne(new Fruit(null, "Malayan Tapir"));
    idToDelete = createOne(new Fruit(null, "Malayan Tapir"));
  }

  @AfterEach
  public void deleteEntities() {
    deleteOne(idToGet);
    deleteOne(idToUpdate);
    deleteOne(idToDelete);
  }

  protected Long createOne(Fruit fruit) {
    return Stream.of(URI.create(given()
        .contentType(ContentType.JSON)
        .body(fruit)
        .post("/fruits")
        .then()
        .extract()
        .header("Location"))
        .getPath()
        .split("/"))
        .reduce((first, second) -> second)
        .map(Long::parseLong)
        .orElse(null);
  }

  protected void deleteOne(Long id) {
    given()
        .contentType(ContentType.JSON)
        .delete("/fruits/{id}", id);
  }

  @Test
  public void getAll() {
    get("/fruits")
        .then()
        .statusCode(200)
        .body("id", hasItems(idToGet.intValue(), idToUpdate.intValue()));
  }

  @Test
  public void getSingle() {
    get("/fruits/{id}", idToGet)
        .then()
        .statusCode(200)
        .body("id", equalTo(idToGet.intValue()));
  }

  @Test
  public void getSingleMissing() {
    get("/fruits/{id}", -404L)
        .then()
        .statusCode(404);
  }

  @Test
  public void create() {
    Fruit body = new Fruit(null, "Malayan Tapir");
    given()
        .contentType(ContentType.JSON)
        .body(body)
        .post("/fruits")
        .then()
        .statusCode(201);
  }

  @Test
  public void update() {
    Fruit body = new Fruit(idToUpdate, "Malayan Tapir");
    given()
        .contentType(ContentType.JSON)
        .body(body)
        .put("/fruits/{id}", idToUpdate)
        .then()
        .statusCode(200);
  }

  @Test
  public void updateMissing() {
    Fruit body = new Fruit(-42L, "Malayan Tapir");
    given()
        .contentType(ContentType.JSON)
        .body(body)
        .put("/fruits/{id}", -42L)
        .then()
        .statusCode(404);
  }

  @Test
  public void deleteSingle() {
    delete("/fruits/{id}", idToDelete)
        .then()
        .statusCode(204);
  }

  @Test
  public void deleteSingleMissing() {
    delete("/fruits/{id}", -404L)
        .then()
        .statusCode(404);
  }
}
