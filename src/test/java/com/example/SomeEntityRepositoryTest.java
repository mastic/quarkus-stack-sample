package com.example;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import io.quarkus.test.junit.QuarkusTest;
import io.vertx.mutiny.sqlclient.Pool;
import java.time.Duration;
import java.util.List;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class SomeEntityRepositoryTest {

  private static final Duration TIMEOUT = Duration.ofSeconds(2);

  @Inject
  Pool client;

  @Inject
  SomeEntityRepository repository;

  private Long idToGet, idToUpdate, idToDelete;

  @BeforeEach
  public void initDatabase() {
    client.query("SELECT * FROM SOME_ENTITY")
        .execute()
        .onFailure()
        .recoverWithUni(() -> client
            .query("CREATE TABLE SOME_ENTITY (id SERIAL PRIMARY KEY, f1 INTEGER, f2 BOOLEAN, f3 NVARCHAR, f4 TIMESTAMP)")
            .execute())
        .await()
        .indefinitely();
    idToGet = createOne(new SomeEntity(null, 17983, false, SomeType.values()[0], java.time.Instant.now()));
    idToUpdate = createOne(new SomeEntity(null, 17983, false, SomeType.values()[0], java.time.Instant.now()));
    idToDelete = createOne(new SomeEntity(null, 17983, false, SomeType.values()[0], java.time.Instant.now()));
  }

  @AfterEach
  public void deleteEntities() {
    deleteOne(idToGet);
    deleteOne(idToUpdate);
    deleteOne(idToDelete);
  }

  protected Long createOne(SomeEntity someEntity) {
    return repository.insert(someEntity)
        .map(inserted -> inserted.id)
        .await()
        .atMost(TIMEOUT);
  }

  protected void deleteOne(Long id) {
    repository.deleteById(id)
        .await()
        .atMost(TIMEOUT);
  }

  @Test
  public void getAll() {
    List<Long> ids = repository.findAll()
        .map(item -> item.id)
        .collectItems().asList()
        .await().atMost(TIMEOUT);
    assertThat(ids, hasItems(idToGet, idToUpdate));
  }

  @Test
  public void getSingle() {
    Long id = repository.findById(idToGet)
        .map(item -> item.id)
        .await().atMost(TIMEOUT);
    assertThat(id, is(idToGet));
  }

  @Test
  public void getSingleMissing() {
    SomeEntity someEntity = repository.findById(-404L)
        .await().atMost(TIMEOUT);
    assertThat(someEntity, nullValue());
  }

  @Test
  public void create() {
    SomeEntity someEntity = new SomeEntity(null, 17983, false, SomeType.values()[0], java.time.Instant.now());

    SomeEntity created = repository.insert(someEntity)
        .await().atMost(TIMEOUT);

    assertThat(created, notNullValue());
    assertThat(created.id, notNullValue());
  }


  @Test
  public void update() {
    SomeEntity someEntity = new SomeEntity(idToUpdate, 18025, true, SomeType.values()[0], java.time.Instant.now());

    assertThat(repository.update(someEntity)
        .await().atMost(TIMEOUT), is(true));

    SomeEntity updated = repository.findById(idToUpdate)
        .await().atMost(TIMEOUT);

    assertThat(updated, notNullValue());
    assertThat(updated.f1, equalTo(someEntity.f1));
    assertThat(updated.f2, equalTo(someEntity.f2));
    assertThat(updated.f3, equalTo(someEntity.f3));
    assertThat(updated.f4, equalTo(someEntity.f4));
  }

  @Test
  public void updateMissing() {
    SomeEntity missing = new SomeEntity(-42L, 17983, false, SomeType.values()[0], java.time.Instant.now());
    assertThat(repository.update(missing)
        .await().atMost(TIMEOUT), is(false));
  }

  @Test
  public void deleteSingle() {
    assertThat(repository.deleteById(idToDelete)
        .await().atMost(TIMEOUT), is(true));

    assertThat(repository.findById(idToDelete)
        .await().atMost(TIMEOUT), nullValue());
  }

  @Test
  public void deleteSingleMissing() {
    assertThat(repository.findById(-404L)
        .await().atMost(TIMEOUT), nullValue());
  }
}
