package com.example;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import io.quarkus.test.junit.QuarkusTest;
import io.vertx.mutiny.sqlclient.Pool;
import java.time.Duration;
import java.util.List;
import javax.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@QuarkusTest
public class FruitRepositoryTest {

  private static final Duration TIMEOUT = Duration.ofSeconds(2);

  @Inject
  Pool client;

  @Inject
  FruitRepository repository;

  private Long idToGet, idToUpdate, idToDelete;

  @BeforeEach
  public void initDatabase() {
    client.query("SELECT * FROM FRUIT")
        .execute()
        .onFailure()
        .recoverWithUni(() -> client
            .query("CREATE TABLE FRUIT (id SERIAL PRIMARY KEY, name TEXT NOT NULL)")
            .execute())
        .await()
        .indefinitely();
    idToGet = createOne(new Fruit(null, "Malayan Tapir"));
    idToUpdate = createOne(new Fruit(null, "Malayan Tapir"));
    idToDelete = createOne(new Fruit(null, "Malayan Tapir"));
  }

  @AfterEach
  public void deleteEntities() {
    deleteOne(idToGet);
    deleteOne(idToUpdate);
    deleteOne(idToDelete);
  }

  protected Long createOne(Fruit fruit) {
    return repository.insert(fruit)
        .map(inserted -> inserted.id)
        .await()
        .atMost(TIMEOUT);
  }

  protected void deleteOne(Long id) {
    repository.deleteById(id)
        .await()
        .atMost(TIMEOUT);
  }

  @Test
  public void getAll() {
    List<Long> ids = repository.findAll()
        .map(item -> item.id)
        .collectItems().asList()
        .await().atMost(TIMEOUT);
    assertThat(ids, hasItems(idToGet, idToUpdate));
  }

  @Test
  public void getSingle() {
    Long id = repository.findById(idToGet)
        .map(item -> item.id)
        .await().atMost(TIMEOUT);
    assertThat(id, is(idToGet));
  }

  @Test
  public void getSingleMissing() {
    Fruit fruit = repository.findById(-404L)
        .await().atMost(TIMEOUT);
    assertThat(fruit, nullValue());
  }

  @Test
  public void create() {
    Fruit fruit = new Fruit(null, "Malayan Tapir");

    Fruit created = repository.insert(fruit)
        .await().atMost(TIMEOUT);

    assertThat(created, notNullValue());
    assertThat(created.id, notNullValue());
  }


  @Test
  public void update() {
    Fruit fruit = new Fruit(idToUpdate, "Malayan Tapir Updated");

    assertThat(repository.update(fruit)
        .await().atMost(TIMEOUT), is(true));

    Fruit updated = repository.findById(idToUpdate)
        .await().atMost(TIMEOUT);

    assertThat(updated, notNullValue());
    assertThat(updated.name, equalTo(fruit.name));
  }

  @Test
  public void updateMissing() {
    Fruit missing = new Fruit(-42L, "Malayan Tapir");
    assertThat(repository.update(missing)
        .await().atMost(TIMEOUT), is(false));
  }

  @Test
  public void deleteSingle() {
    assertThat(repository.deleteById(idToDelete)
        .await().atMost(TIMEOUT), is(true));

    assertThat(repository.findById(idToDelete)
        .await().atMost(TIMEOUT), nullValue());
  }

  @Test
  public void deleteSingleMissing() {
    assertThat(repository.findById(-404L)
        .await().atMost(TIMEOUT), nullValue());
  }
}
