package com.example;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class SomeEntity {

  public Long id;
  
  public Integer f1;

  public Boolean f2;

  public SomeType f3;

  public java.time.Instant f4;

  public SomeEntity() {
  }

  public SomeEntity(Long id, Integer f1 , Boolean f2 , SomeType f3 , java.time.Instant f4) {
    this.id = id;
    this.f1 = f1;
    this.f2 = f2;
    this.f3 = f3;
    this.f4 = f4;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("id", id)
        .append("f1", f1)
        .append("f2", f2)
        .append("f3", f3)
        .append("f4", f4)
        .toString();
  }
}
