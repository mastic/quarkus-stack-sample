package com.example;

import static java.time.ZoneOffset.UTC;

import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.SqlClient;
import io.vertx.mutiny.sqlclient.Tuple;
import java.time.LocalDateTime;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class SomeEntityRepository extends ReactiveCrudRepository<SomeEntity, Long> {

  @Inject
  PgPool client;

  public SomeEntityRepository() {
    super(
        "SELECT id, f1, f2, f3, f4 FROM SOME_ENTITY",
        "SELECT id, f1, f2, f3, f4 FROM SOME_ENTITY WHERE id = $1",
        "INSERT INTO SOME_ENTITY (f1, f2, f3, f4) VALUES ($1, $2, $3, $4) RETURNING id, f1, f2, f3, f4",
        "UPDATE SOME_ENTITY SET f1 = $1, f2 = $2, f3 = $3, f4 = $4 WHERE id = $5",
        "DELETE FROM SOME_ENTITY WHERE id = $1"
    );
  }

  @Override
  protected SomeEntity from(Row row) {
    return new SomeEntity(row.getLong("id"),
        row.getInteger("f1"),
        row.getBoolean("f2"),
        SomeType.valueOf(row.getString("f3")),
        row.getLocalDateTime("f4").atZone(UTC).toInstant());
  }

  @Override
  protected SqlClient client() {
    return client;
  }

  @Override
  protected Tuple tupleWithoutId(SomeEntity entity) {
    return Tuple.tuple()
        .addInteger(entity.f1)
        .addBoolean(entity.f2)
        .addString(entity.f3.toString())
        .addLocalDateTime(LocalDateTime.ofInstant(entity.f4, UTC))
        ;
  }

  @Override
  protected Tuple tupleAndId(SomeEntity entity) {
    return Tuple.tuple()
        .addInteger(entity.f1)
        .addBoolean(entity.f2)
        .addString(entity.f3.toString())
        .addLocalDateTime(LocalDateTime.ofInstant(entity.f4, UTC))
        .addLong(entity.id)
        ;
  }

}
