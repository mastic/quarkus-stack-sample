package com.example;

import static java.time.ZoneOffset.UTC;

import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.SqlClient;
import io.vertx.mutiny.sqlclient.Tuple;
import java.time.LocalDateTime;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class FruitRepository extends ReactiveCrudRepository<Fruit, Long> {

  @Inject
  PgPool client;

  public FruitRepository() {
    super(
        "SELECT id, name FROM FRUIT",
        "SELECT id, name FROM FRUIT WHERE id = $1",
        "INSERT INTO FRUIT (name) VALUES ($1) RETURNING id, name",
        "UPDATE FRUIT SET name = $1 WHERE id = $2",
        "DELETE FROM FRUIT WHERE id = $1"
    );
  }

  @Override
  protected Fruit from(Row row) {
    return new Fruit(row.getLong("id"),
        row.getString("name"));
  }

  @Override
  protected SqlClient client() {
    return client;
  }

  @Override
  protected Tuple tupleWithoutId(Fruit entity) {
    return Tuple.tuple()
        .addString(entity.name)
        ;
  }

  @Override
  protected Tuple tupleAndId(Fruit entity) {
    return Tuple.tuple()
        .addString(entity.name)
        .addLong(entity.id)
        ;
  }

}
