package com.example;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import java.net.URI;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("fruits")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FruitResource {

  @Inject
  FruitRepository store;

  @GET
  public Multi<Fruit> getAll() {
    return store.findAll();
  }

  @GET
  @Path("{id}")
  public Uni<Response> getSingle(@PathParam Long id) {
    return store.findById(id)
        .map(fruit -> fruit != null ? Response.ok(fruit) : Response.status(Status.NOT_FOUND))
        .map(ResponseBuilder::build);
  }

  @POST
  public Uni<Response> create(Fruit fruit) {
    return store.insert(fruit)
        .map(f -> URI.create("/fruits/" + f.id))
        .map(uri -> Response.created(uri).build());
  }

  @PUT
  @Path("{id}")
  public Uni<Response> update(@PathParam Long id, Fruit fruit) {
    fruit.id = id;
    return store.update(fruit)
        .map(updated -> updated ? Status.OK : Status.NOT_FOUND)
        .map(status -> Response.status(status).build());
  }

  @DELETE
  @Path("{id}")
  public Uni<Response> delete(@PathParam Long id) {
    return store.deleteById(id)
        .map(deleted -> deleted ? Status.NO_CONTENT : Status.NOT_FOUND)
        .map(status -> Response.status(status).build());
  }
}
